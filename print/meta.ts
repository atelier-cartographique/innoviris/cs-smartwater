import { makeRecord } from "sdi/source";
import { fromRecord } from "sdi/locale";

export function attribution() {
  return "SmartWater Brussels";
}

export function credits() {
  return fromRecord(
    makeRecord(
      "Fond de plan: Brussels UrbIS ®© - CIRB - CIBG",
      "Achtergrond: Brussels UrbIS ®© - CIRB - CIBG"
    )
  );
}
